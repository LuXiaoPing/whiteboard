package WBView;

import java.util.Timer;
import java.util.TimerTask;

import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.Cocos2dxHelper.Cocos2dxHelperListener;
import org.cocos2dx.lib.Cocos2dxRenderer;
import WBTools.WBEngine;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;
import cocos2dx.WB.WBCallBack;


//----------------change-----------------
//由继承Activity类改为继承FrameLayout 继承了父类View 可以造成一个组件进行显示
public class WBView extends FrameLayout implements Cocos2dxHelperListener{

	
	static {
        System.loadLibrary("cocos2dcpp");
    }


	private WBGLSurfaceView mGLSurfaceView;
	

	private final  Handler handler=new Handler(){
			@Override
			public void handleMessage(Message msg)
			{
				switch (msg.what) {
				case 0x2:
					replaceSurfaceView();
					break;
				case 0x3:
					setVisibility(View.INVISIBLE);
					break;
				case 0x4:
					setVisibility(View.VISIBLE);
					break;
				case 0x05:
					
					break;
				default:
					break;
				}
			}
		};
	
	//-----------------add-------------------
	private float frameWidth; 
	private float frameHeight; 
	private float resolutionWidth; 
	private float resolutionHeight;
	private WBCallBack callBack0;

	//--------change---------------
	public WBView(Context context) {
		super(context);
		//this.init();
		Cocos2dxHelper.init(context, this);		
		setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	}

	//--------change---------------
	public WBView(Context context,AttributeSet set)
	{
		super(context,set);
		//------------add----------------
		//this.init();
		Cocos2dxHelper.init(context, this);
		setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	}
	
	@Override
	public void runOnGLThread(final Runnable pRunnable) {
		this.mGLSurfaceView.queueEvent(pRunnable);
	}
	@Override
	public void showDialog(final String pTitle, final String pMessage) {
	}

	@Override
	public void showEditTextDialog(final String pTitle, final String pContent, final int pInputMode, final int pInputFlag, final int pReturnType, final int pMaxLength) { 

	}
	
	public void createWB(final float frameWidth,final float frameHeight,final float resolutionWidth,final float resolutionHeight,final WBCallBack callback)
	{
		setVisibility(View.INVISIBLE);
		this.frameHeight=frameHeight; this.frameWidth=frameWidth; this.resolutionHeight=resolutionHeight; this.resolutionWidth=resolutionWidth;
		this.callBack0=callback;
		//调用定时器是防止出现重建白板时发生黑影问题
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				handler.sendEmptyMessage(0x2);		
			}
		}, 10);

	}
	
	private void replaceSurfaceView()
	{
		WBEngine.removeWBDrawBackground();
		removeAllViews();
		this.mGLSurfaceView=new WBGLSurfaceView(getContext());
        this.mGLSurfaceView.setCocos2dxRenderer(new Cocos2dxRenderer());
        this.mGLSurfaceView.setLayoutParams(new ViewGroup.LayoutParams((int)frameWidth,(int)frameHeight));
 		
		addView(this.mGLSurfaceView);

		this.mGLSurfaceView.setListener(new WBCallBack() {		
			@Override
			public void onSuccess() {
				WBEngine.setPCWBViewSize(frameWidth, frameHeight, resolutionWidth, resolutionHeight);		
				handler.sendEmptyMessage(0x4);
				if(callBack0!=null)
					callBack0.onSuccess();
			}
			
			@Override
			public void onError() {
				if(callBack0!=null)
					callBack0.onError();
			}
		});	
	}

	public void changeSize(ViewGroup.LayoutParams layoutParams)
	{
		pauseRender();//设置白板被动渲染
		this.mGLSurfaceView.setLayoutParams(layoutParams);
	}

	public void pauseRender()
	{
		this.mGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	public void resumeRender()
	{
		this.mGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
	}
}
