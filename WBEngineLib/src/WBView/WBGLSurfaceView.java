package WBView;

import org.cocos2dx.lib.Cocos2dxAccelerometer;
import org.cocos2dx.lib.Cocos2dxRenderer;

import android.content.Context;
import cocos2dx.WB.*;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

public class WBGLSurfaceView extends GLSurfaceView{

	// ===========================================================
	// Constants
	// ===========================================================

	private static final String TAG = WBGLSurfaceView.class.getSimpleName();

	private final static int HANDLER_OPEN_IME_KEYBOARD = 2;
	private final static int HANDLER_CLOSE_IME_KEYBOARD = 3;

	// ===========================================================
	// Fields
	// ===========================================================

	// TODO Static handler -> Potential leak!
	private static Handler sHandler;

	private static WBGLSurfaceView mWBGLSurfaceView;
	private static WBTextInputWraper sCocos2dxTextInputWraper;

	private Cocos2dxRenderer mCocos2dxRenderer;
	private WBEditText mCocos2dxEditText;

	// ===========================================================
	// Constructors
	// ===========================================================
	
	//---------------add---------------------------------------------
	public void setListener(WBCallBack callback)
	{
		this.mCocos2dxRenderer.setListener(callback);
	}
	

	public WBGLSurfaceView(final Context context) {
		super(context);
		this.initView();
	}

	public WBGLSurfaceView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		
		this.initView();
	}

	protected void initView() {
		this.setEGLContextClientVersion(2);
		this.setFocusableInTouchMode(true);
		WBGLSurfaceView.mWBGLSurfaceView = this;
		WBGLSurfaceView.sCocos2dxTextInputWraper = new WBTextInputWraper(this);

		/*
		WBGLSurfaceView.sHandler = new Handler() {
			@Override
			public void handleMessage(final Message msg) {
				switch (msg.what) {
					case HANDLER_OPEN_IME_KEYBOARD:
						if (null != WBGLSurfaceView.this.mCocos2dxEditText && WBGLSurfaceView.this.mCocos2dxEditText.requestFocus()) {
							WBGLSurfaceView.this.mCocos2dxEditText.removeTextChangedListener(WBGLSurfaceView.sCocos2dxTextInputWraper);
							WBGLSurfaceView.this.mCocos2dxEditText.setText("");
							final String text = (String) msg.obj;
							WBGLSurfaceView.this.mCocos2dxEditText.append(text);
							WBGLSurfaceView.sCocos2dxTextInputWraper.setOriginText(text);
							WBGLSurfaceView.this.mCocos2dxEditText.addTextChangedListener(WBGLSurfaceView.sCocos2dxTextInputWraper);
							final InputMethodManager imm = (InputMethodManager) WBGLSurfaceView.mWBGLSurfaceView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(WBGLSurfaceView.this.mCocos2dxEditText, 0);
							Log.d("GLSurfaceView", "showSoftInput");
						}
						break;

					case HANDLER_CLOSE_IME_KEYBOARD:
						if (null != WBGLSurfaceView.this.mCocos2dxEditText) {
							WBGLSurfaceView.this.mCocos2dxEditText.removeTextChangedListener(WBGLSurfaceView.sCocos2dxTextInputWraper);
							final InputMethodManager imm = (InputMethodManager) WBGLSurfaceView.mWBGLSurfaceView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(WBGLSurfaceView.this.mCocos2dxEditText.getWindowToken(), 0);
							WBGLSurfaceView.this.requestFocus();
							Log.d("GLSurfaceView", "HideSoftInput");
						}
						break;
				}
			}
		};*/
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================


    public static WBGLSurfaceView getInstance() {
	   return mWBGLSurfaceView;
       }

    public static void queueAccelerometer(final float x, final float y, final float z, final long timestamp) {	
	   mWBGLSurfaceView.queueEvent(new Runnable() {
		@Override
		    public void run() {
			    Cocos2dxAccelerometer.onSensorChanged(x, y, z, timestamp);
		}
	    });
	}

	public void setCocos2dxRenderer(final Cocos2dxRenderer renderer) {
		this.mCocos2dxRenderer = renderer;
		this.setRenderer(this.mCocos2dxRenderer);
	}

	
	private String getContentText() {
		return this.mCocos2dxRenderer.getContentText();
	}
	
	//------------------change--------------------
	
	public WBEditText getCocos2dxEditText() {
		return this.mCocos2dxEditText;
	}
	
	//-------------change---------------------------------
	
	public void setCocos2dxEditText(final WBEditText pCocos2dxEditText) {
		this.mCocos2dxEditText = pCocos2dxEditText;
		if (null != this.mCocos2dxEditText && null != WBGLSurfaceView.sCocos2dxTextInputWraper) {
			this.mCocos2dxEditText.setOnEditorActionListener(WBGLSurfaceView.sCocos2dxTextInputWraper);
			this.mCocos2dxEditText.setWBGLSurfaceView(this);
			this.requestFocus();
		}
	}
	
	

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public void onResume() {
		super.onResume();
		
		//this.setRenderMode(RENDERMODE_CONTINUOUSLY);
		
		this.queueEvent(new Runnable() {
			@Override
			public void run() {
				WBGLSurfaceView.this.mCocos2dxRenderer.handleOnResume();
			}
		});
	}

	@Override
	public void onPause() {
		this.queueEvent(new Runnable() {
			@Override
			public void run() {
				WBGLSurfaceView.this.mCocos2dxRenderer.handleOnPause();
			}
		});
		
		this.setRenderMode(RENDERMODE_WHEN_DIRTY);
		//super.onPause();
	}

	@Override
	public boolean onTouchEvent(final MotionEvent pMotionEvent) {
		// these data are used in ACTION_MOVE and ACTION_CANCEL
		final int pointerNumber = pMotionEvent.getPointerCount();
		final int[] ids = new int[pointerNumber];
		final float[] xs = new float[pointerNumber];
		final float[] ys = new float[pointerNumber];

		for (int i = 0; i < pointerNumber; i++) {
			ids[i] = pMotionEvent.getPointerId(i);
			xs[i] = pMotionEvent.getX(i);
			ys[i] = pMotionEvent.getY(i);
		}

		switch (pMotionEvent.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_POINTER_DOWN:
				final int indexPointerDown = pMotionEvent.getAction() >> MotionEvent.ACTION_POINTER_ID_SHIFT;
				final int idPointerDown = pMotionEvent.getPointerId(indexPointerDown);
				final float xPointerDown = pMotionEvent.getX(indexPointerDown);
				final float yPointerDown = pMotionEvent.getY(indexPointerDown);

				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionDown(idPointerDown, xPointerDown, yPointerDown);
					}
				});
				break;

			case MotionEvent.ACTION_DOWN:
				// there are only one finger on the screen
				final int idDown = pMotionEvent.getPointerId(0);
				final float xDown = xs[0];
				final float yDown = ys[0];

				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionDown(idDown, xDown, yDown);
					}
				});
				break;

			case MotionEvent.ACTION_MOVE:
				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionMove(ids, xs, ys);
					}
				});
				break;

			case MotionEvent.ACTION_POINTER_UP:
				final int indexPointUp = pMotionEvent.getAction() >> MotionEvent.ACTION_POINTER_ID_SHIFT;
				final int idPointerUp = pMotionEvent.getPointerId(indexPointUp);
				final float xPointerUp = pMotionEvent.getX(indexPointUp);
				final float yPointerUp = pMotionEvent.getY(indexPointUp);

				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionUp(idPointerUp, xPointerUp, yPointerUp);
					}
				});
				break;

			case MotionEvent.ACTION_UP:
				// there are only one finger on the screen
				final int idUp = pMotionEvent.getPointerId(0);
				final float xUp = xs[0];
				final float yUp = ys[0];

				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionUp(idUp, xUp, yUp);
					}
				});
				break;

			case MotionEvent.ACTION_CANCEL:
				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleActionCancel(ids, xs, ys);
					}
				});
				break;
		}

        /*
		if (BuildConfig.DEBUG) {
			WBGLSurfaceView.dumpMotionEvent(pMotionEvent);
		}
		*/
		return false;
	}

	/*
	 * This function is called before Cocos2dxRenderer.nativeInit(), so the
	 * width and height is correct.
	 */
	@Override
	protected void onSizeChanged(final int pNewSurfaceWidth, final int pNewSurfaceHeight, final int pOldSurfaceWidth, final int pOldSurfaceHeight) {
		if(!this.isInEditMode()) {
			this.mCocos2dxRenderer.setScreenWidthAndHeight(pNewSurfaceWidth, pNewSurfaceHeight);
		}
	}

	@Override
	public boolean onKeyDown(final int pKeyCode, final KeyEvent pKeyEvent) {
		switch (pKeyCode) {
			case KeyEvent.KEYCODE_BACK:
			case KeyEvent.KEYCODE_MENU: 
				this.queueEvent(new Runnable() {
					@Override
					public void run() {
						WBGLSurfaceView.this.mCocos2dxRenderer.handleKeyDown(pKeyCode);
					}
				});
				return false;
			default:
				return super.onKeyDown(pKeyCode, pKeyEvent);
		}
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	public static void openIMEKeyboard() {
		final Message msg = new Message();
		msg.what = WBGLSurfaceView.HANDLER_OPEN_IME_KEYBOARD;
		msg.obj = WBGLSurfaceView.mWBGLSurfaceView.getContentText();
		WBGLSurfaceView.sHandler.sendMessage(msg);
	}

	public static void closeIMEKeyboard() {
		final Message msg = new Message();
		msg.what = WBGLSurfaceView.HANDLER_CLOSE_IME_KEYBOARD;
		WBGLSurfaceView.sHandler.sendMessage(msg);
	}

	public void insertText(final String pText) {
		this.queueEvent(new Runnable() {
			@Override
			public void run() {
				WBGLSurfaceView.this.mCocos2dxRenderer.handleInsertText(pText);
			}
		});
	}

	public void deleteBackward() {
		this.queueEvent(new Runnable() {
			@Override
			public void run() {
				WBGLSurfaceView.this.mCocos2dxRenderer.handleDeleteBackward();
			}
		});
	}

	private static void dumpMotionEvent(final MotionEvent event) {
		final String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
		final StringBuilder sb = new StringBuilder();
		final int action = event.getAction();
		final int actionCode = action & MotionEvent.ACTION_MASK;
		sb.append("event ACTION_").append(names[actionCode]);
		if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
			sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
			sb.append(")");
		}
		sb.append("[");
		for (int i = 0; i < event.getPointerCount(); i++) {
			sb.append("#").append(i);
			sb.append("(pid ").append(event.getPointerId(i));
			sb.append(")=").append((int) event.getX(i));
			sb.append(",").append((int) event.getY(i));
			if (i + 1 < event.getPointerCount()) {
				sb.append(";");
			}
		}
		sb.append("]");
		Log.d(WBGLSurfaceView.TAG, sb.toString());
	}
}
