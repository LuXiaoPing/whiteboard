/**
 * 
 */
package WBView;

import org.cocos2dx.lib.Cocos2dxRenderer;

import cocos2dx.WB.WBCallBack;
import WBTools.WBEngine;
import android.content.Context;
import android.util.AttributeSet;

/**
 * @author wghuang
 * @version 2015-1-23 ����10:11:51
 */
public class WBCanvas extends WBGLSurfaceView {

	static {
        System.loadLibrary("cocos2dcpp");
    }

	/**
	 * @param context
	 */
	public WBCanvas(Context context) {
		super(context);
		init();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public WBCanvas(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init() {
		setCocos2dxRenderer(new Cocos2dxRenderer());
	}

	
	public void createWB(final float frameWidth,final float frameHeight,final float resolutionWidth,final float resolutionHeight,final WBCallBack callback){
		setListener(new WBCallBack() {		
			@Override
			public void onSuccess() {
				WBEngine.setPCWBViewSize(frameWidth, frameHeight, resolutionWidth, resolutionHeight);		
				if(callback!=null)
					callback.onSuccess();
			}
			
			@Override
			public void onError() {
				if(callback!=null)
					callback.onError();
			}
		});	
	}
}
