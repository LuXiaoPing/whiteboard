package WBTools;

public class WBEngine {
	
	//传入XML数据
	public static  native void addWBDrawNode(String nodexml);
	
	
	//去除指定的XML数据
	public static native void removeWBDrawNodeByName(String nodeName);
	
	//清除所有
	public static native void removeAllWBDrawNode();
	
	
	//设定尺寸 前两个是物理单位是像素px 后两个是相对应的逻辑单位dp
	public static native void setPCWBViewSize(float framWidth,float frameHeight, float resolutionWidth, float resolutionHeight);

	//设置背景图片
	public static native void setWBDrawBackground(String imageFile);
	//清楚背景图片
	public static native void removeWBDrawBackground();
	
	//public static native boolean stopWBEngine();
}
