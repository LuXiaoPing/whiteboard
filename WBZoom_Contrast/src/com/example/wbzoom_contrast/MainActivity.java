package com.example.wbzoom_contrast;

import cocos2dx.WB.WBCallBack;
import WBTools.WBEngine;
import WBView.WBView;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;

public class MainActivity extends Activity {
	/*
	 * 调整ZOOMRANDOM(随机梯度[0~1)*GRADIENT),GRADIENT(梯度值),DELAYMILLIS(改变时间间隔,单位毫秒),查看效果
	 */
	final static private boolean ZOOMRANDOM = true;
	final private int GRADIENT = 20;
	final private long DELAYMILLIS = 70;

	private WBView viewWB;
	private View viewExample;
	
	private int containerWidth;
	private int containerHeight;
	
	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			int value = msg.what;
//			value = (int) (Math.random() * value + 1);
//			System.out.println("MainActivity.enclosing_method(),value:" + value);
			android.view.ViewGroup.LayoutParams layoutParams = viewExample.getLayoutParams();
			layoutParams.width += value;
			layoutParams.height += value;
			if (layoutParams.width > containerWidth || layoutParams.height > containerHeight) {
				layoutParams.width = containerWidth;
				layoutParams.height = containerHeight;
				value = -Math.abs(value);
			}
			
			if (layoutParams.width < 0 || layoutParams.height < 0) {
				layoutParams.width = 0;
				layoutParams.height = 0;
				value = Math.abs(value);
			}
			
			//viewWB.setLayoutParams(layoutParams);
			viewWB.changeSize(layoutParams);
			
			viewExample.setLayoutParams(layoutParams);
			
			if (ZOOMRANDOM) {
				if (value > 0) {
					value = (int) (Math.random() * GRADIENT);
					if (value == 0) {
						value = 1;
					}
				} else {
					value = (int) (Math.random() * -GRADIENT);
					if (value == 0) {
						value = -1;
					}
				}
			}
			//			handler.sendEmptyMessage(value);
			handler.sendEmptyMessageDelayed(value, DELAYMILLIS);
			
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		viewWB = (WBView) findViewById(R.id.wbcanvas);
		viewExample = findViewById(R.id.view_example);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		containerWidth = findViewById(R.id.fl_container).getWidth();
		containerHeight = findViewById(R.id.fl_container).getHeight();
		System.out.println("MainActivity.onOptionsItemSelected(),width:" + containerWidth + ",height:" + containerHeight);
		System.out.println("MainActivity.onOptionsItemSelected(),width:" + findViewById(R.id.fl_container).getLayoutParams().width);
		if (item.getItemId() == R.id.action_zoom_loop) {
			handler.sendEmptyMessage(-GRADIENT);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("MainActivity.onResume(),viewExamle,widht:" + viewExample.getWidth());
	}
	
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		System.out.println("MainActivity.onAttachedToWindow(),viewExamle,widht:" + viewExample.getWidth());
		float frameWidth = 640;
		float frameHeight = 480;
		viewWB.createWB(frameWidth, frameHeight, 1600, 1200, new WBCallBack() {
			
			@Override
			public void onSuccess() {
				WBEngine.setWBDrawBackground("/mnt/sdcard/a243115597_0.jpg");
				WBEngine.addWBDrawNode(rectangle);
				WBEngine.addWBDrawNode(circular);
			}
			
			@Override
			public void onError() {}
		});
		
		android.view.ViewGroup.LayoutParams layoutParams = viewExample.getLayoutParams();
		layoutParams.width = (int) frameWidth;
		layoutParams.height = (int) frameHeight;
		viewExample.setLayoutParams(layoutParams);
		
	}

	private static final String rectangle = "<object><type>rect</type><color>255</color><points><point x=\"4\" y=\"4\"/><point x=\"1594\" y=\"1194\" type=\"x\"/></points><owner>Z{A9D87E1D-5C8A-4341-BA7A-1568E6746151}</owner><zorder>4</zorder><name>A00004</name><zorder>4</zorder><name>A00004</name></object>";
	private static final String circular = "<object><type>round</type><color>255</color><points><point x=\"690\" y=\"474\"/><point x=\"1054\" y=\"842\" type=\"x\"/></points><owner>Z{7D156194-3519-4ADC-BC09-5D3992DA0FC9}</owner><zorder>5</zorder><name>A00005</name><zorder>5</zorder><name>A00005</name></object>";
	
    private final String textXML="<object>  <type>text</type>  <color>6736998</color>  <text>中国好</text>  "+ 
    "<font bold=\"x\"  charset=\"1\"  color=\"6736998\" height=\"-12\""+
    "  italic=\"x\" name=\"&#x5B8B;&#x4F53;\" pitch=\"0\"  size=\"70\" strikeOut=\"x\" underline=\"x\"/>"+
    "<points> <point x=\"300\" y=\"105\"/></points> "+
    " <owner>Z{F3FCDB91-66C3-4A32-80B0-CF7293401DA8}</owner>  <zorder>13</zorder>"+
    "  <name>A00013</name>  <zorder>13</zorder>  <name>A00013</name>  </object>";
}
